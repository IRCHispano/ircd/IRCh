# Changelog
Todos los cambios notables de este proyecto deben ser documentados en este archivo.

El formato esta basado en [Mantenga un ChangeLog](https://keepachangelog.com/es-ES/1.0.0/),
y este proyecto se adhiere a [Versionado Semántico](https://semver.org/lang/es).

## [Unreleased]
### Added
 * Soporte de PROXY-HAProxy.
 * Comando BMODE para cambios de modos de canal mediante bot virtual.
 * Comando GHOST para desconectar conexiones caídas.
 * Soporte de comandos SVS*
   * SVSNICK, para cambiar el nick de un usuario.
   * SVSMODES, para cambiar modos de un usuario.
   * SVSJOIN, para meter un usuario en un canal.
   * SVSPART, para sacar un usuario de un canal.
 * Soporte de DDB (Distributed DataBase) con lo siguiente:
   * Comando DB para distribuir registros por la red.
   * Comando DBQ para hacer consultas.
   * Tablas:
      * b, Bots virtuales.
      * f, Features globales.
      * m, MOTD para partes globales dentro de MOTD normales.
      * n, Nicks registrados.
      * o, Operardores.
   * Eventos al añadir, actualizar o eliminar registros para siguientes tablas:
      * features.
      * nicks.
      * operadores.
 * Nuevos Features para configuración:
   * ALLOW_RANDOM_NICKS, se permite poner un nick aleatorio con */nick **.
   * ALLOW_SUSPEND_NICKS, se permite poner un nick suspendido.
   * NOIDENT, para deshabilitar el chequeo de ident.
   * HIS_SERVERS, para esconder servidores de clientes.
   * PREFIX_RANDOM_NICKS, prefijo de los nicks aleatorios.
 * Soporte de Excepción de k-line/g-line.
 * Nuevos modos de canal:
   * +M, sólo usuarios con +r pueden entrar al canal.
   * +N, no se permite notices al canal, incluidos wallchops (onotice) y wallvoices. 
   * +O, sólo Ircops pueden entrar al canal.
   * +u, no se muestran mensajes de part/quit.
   * +W, solo pueden escriben usuarios Webchat (webirc) o voices o op
   * +z, sólo usuarios con +z pueden entrar al canal.
 * Nuevos modos de usuario:
   * +a, administrador.
   * +b, bot de usuario con docking, salta limite flood, targets y canales.
   * +B, bot Service de la red.
   * +c, limpia colores en canales.
   * +C, coder/desarrollador.
   * +h, helper/ayudante.
   * +I, no idle en el whois vía PRIV.
   * +n, oculta canales en el whois
   * +q, solo recibe mensaje/notice desde usuarios con canales comunes.
   * +R, solo recibe mensaje/notice desde usuarios +r.
   * +S, nick suspendido.
   * +W, notificación de whpos vía PRIV.
   * +X, ve IPs reales vía PRIV.
   * +z, usuario que usa conexión segura (TLS/SSL).
 * Soporte de Monitor (sucesor de Watch).
 * Ampliación del soporte CAP para IRCv3:
   * account-notify
   * away-notfy
   * extended-join
   * invite-notify
   * multi-prefix
   * tls
 * Soporte SSL con comandos STARTTLS y FINGERPRINT.
 * Scripts de configuración rápida y fácil.
 * Modernización de Configure y Makefile.

### Changed
 * Adaptación comandos INVITE, LINKS, USERS, LUSERS y MAP al estilo Hispano.
 * Se amplia el Logging para soportar canales, para que "canten" en un canal determinado.
 * Se cambian referencias de Undernet por Hispano.
 * Se intercambian los modos +r y +R de canal, ya que en Undernet está al revés.
 * Actualización ejemplos de configuración.
 